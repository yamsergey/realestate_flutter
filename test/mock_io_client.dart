import 'dart:io' as io;

final io.HttpClient __defaultDelegate = io.HttpClient();

typedef RequestHandler = Future<String> Function(Uri, io.HttpRequest);
class MockIoHttpClient implements io.HttpClient {
  final RequestHandler handler;

  final io.HttpClient delegate;
  final String host;
  final int port;
  static const String originalUrlHeader = 'X-Mock-Http-Client-Original-URL';

  MockIoHttpClient(this.handler,
      {io.HttpClient? delegate, required io.HttpServer server})
      : this.delegate = delegate ?? __defaultDelegate,
        this.host = server.address.host,
        this.port = server.port {
    server.listen((request) async {
      var response = await handler(
          Uri.parse(request.headers.value(originalUrlHeader)!), request);
      request.response..write(response);
      await request.response.close();
      return;
    });
  }

  static Future<MockIoHttpClient> create(
      RequestHandler handler, {
      io.HttpClient? client,
      io.HttpClient? delegate,
      String host = 'localhost',
      int port = 8081}) async {
    var server = await io.HttpServer.bind(host, port);
    return MockIoHttpClient(handler, server: server, delegate: delegate);
  }

  @override
  Future<io.HttpClientRequest> getUrl(Uri url) async {
    var newUrl = url.replace(scheme: 'http', host: host, port: port);
    var request = await delegate.getUrl(newUrl);
    request.headers.add(originalUrlHeader, url.toString());
    return request;
  }

  @override
  bool autoUncompress = false;

  @override
  Duration? connectionTimeout = Duration(milliseconds: 0);

  @override
  Duration idleTimeout = Duration(milliseconds: 0);

  @override
  int? maxConnectionsPerHost;

  @override
  String? userAgent;

  @override
  void addCredentials(
      Uri url, String realm, io.HttpClientCredentials credentials) {
    // TODO: implement addCredentials
  }

  @override
  void addProxyCredentials(String host, int port, String realm,
      io.HttpClientCredentials credentials) {
    // TODO: implement addProxyCredentials
  }

  @override
  void close({bool force = false}) {
    // TODO: implement close
  }

  @override
  Future<io.HttpClientRequest> delete(String host, int port, String path) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> deleteUrl(Uri url) {
    // TODO: implement deleteUrl
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> get(String host, int port, String path) {
    // TODO: implement get
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> head(String host, int port, String path) {
    // TODO: implement head
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> headUrl(Uri url) {
    // TODO: implement headUrl
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> open(
      String method, String host, int port, String path) {
    // TODO: implement open
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> openUrl(String method, Uri url) {
    // TODO: implement openUrl
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> patch(String host, int port, String path) {
    // TODO: implement patch
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> patchUrl(Uri url) {
    // TODO: implement patchUrl
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> post(String host, int port, String path) {
    // TODO: implement post
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> postUrl(Uri url) {
    // TODO: implement postUrl
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> put(String host, int port, String path) {
    // TODO: implement put
    throw UnimplementedError();
  }

  @override
  Future<io.HttpClientRequest> putUrl(Uri url) {
    // TODO: implement putUrl
    throw UnimplementedError();
  }

  @override
  set authenticate(
          Future<bool> Function(Uri url, String scheme, String realm)? f) =>
      throw UnimplementedError();

  @override
  set authenticateProxy(
          Future<bool> Function(
                  String host, int port, String scheme, String realm)?
              f) =>
      throw UnimplementedError();

  @override
  set badCertificateCallback(
          bool Function(io.X509Certificate cert, String host, int port)?
              callback) =>
      throw UnimplementedError();

  @override
  set findProxy(String Function(Uri url)? f) => delegate.findProxy = f;
}

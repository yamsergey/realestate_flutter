import 'dart:convert';
import 'dart:io';

import 'package:realestate_flutter/network/network.dart';
import 'package:test/test.dart';

void main() {
  test('Should correctly parse json', () async {
    final json = jsonDecode(await File('test/assets/response.json').readAsString())
        as Map<String, dynamic>;

    final response = SearchResponse.fromJson(json);

    expect(response.hasNextPage, false); 
  });
}

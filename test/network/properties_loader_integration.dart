import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:platform_proxy/platform_proxy.dart';
import 'package:platform_proxy/proxy_httpclient.dart';
import 'package:realestate_flutter/main.dart';
import 'package:realestate_flutter/network/client/client.dart';
import 'package:realestate_flutter/network/network.dart';

import '../mock_io_client.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('Should make real dart.io HttpClient request', (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());

    final uri = Uri.parse('https://test.com');
    final mockClient = await MockIoHttpClient.create((url, request) async {
      if (request.method == "GET" && url.host == uri.host) {
        return await File('test/assets/response.json').readAsString();
      } else {
        return '';
      }
    });
    final proxyAwareClient = ProxyAwareHttpClient(
        client: mockClient, platformProxy: PlatformProxy());
    final loader = PropertiesLoader(
        host: 'https://test.com', client: IoHttpClient(proxyAwareClient));
    final result = await loader.list();
    print(result.data);
  });
}

import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart' as dio;
import 'package:http/http.dart' as http;
import 'package:http/testing.dart' as thttp;
import 'package:http_mock_adapter/http_mock_adapter.dart' as dio_mock;
import 'package:realestate_flutter/network/client/client.dart';
import 'package:realestate_flutter/network/network.dart';
import 'package:test/test.dart';

import '../mock_io_client.dart';

void main() {
  test('Mock http Client', () async {
    final uri = Uri.parse('https://test.com');
    final mockClient = thttp.MockClient((request) async {
      if (request.method == "GET" && request.url.host == uri.host) {
        return http.Response(
            await File('test/assets/response.json').readAsString(), 200);
      } else {
        return http.Response('', 404);
      }
    });
    final loader = PropertiesLoader(
        host: 'https://test.com', client: HttpHttpClient(mockClient));
    final result = await loader.list();
    print(result.data);
  });

  test('Mock dart.io HttpClient', () async {
    final uri = Uri.parse('https://test.com');
    final mockClient = await MockIoHttpClient.create((url, request) async {
      if (request.method == "GET" && url.host == uri.host) {
        return await File('test/assets/response.json').readAsString();
      } else {
        return '';
      }
    });
    final loader = PropertiesLoader(
        host: 'https://test.com', client: IoHttpClient(mockClient));
    final result = await loader.list();
    print(result.data);
  });

  test('Mock Dio HttpClient', () async {
    var path = 'https://test.com';
    var mockAdapter = dio_mock.DioAdapter();
    var mockeClient = dio.Dio()..httpClientAdapter = mockAdapter;
    mockAdapter.onGet('$path/properties').reply(200,
        jsonDecode(await File('test/assets/response.json').readAsString()));

    final loader =
        PropertiesLoader(host: path, client: DioHttpCliennt(mockeClient));
    final result = await loader.list();
    print(result);
  });
}

class Response<T> {
  final T? data;

  Response({this.data});

  bool get hasData => data != null;
}

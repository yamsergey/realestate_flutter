import 'dart:convert';
import 'dart:io' as io;

import 'package:dio/dio.dart' as dio;
import 'package:http/http.dart' as http;
import 'package:realestate_flutter/network/client/response.dart';

abstract class HttpClient {
  Future<Response<String>> get(String url);
}

class IoHttpClient extends HttpClient {
  final io.HttpClient client;

  IoHttpClient(this.client);

  @override
  Future<Response<String>> get(String url) async {
    var response = await client.getUrl(Uri.parse(url)).then((e) => e.close());
    var responseString = await utf8.decodeStream(response);
    return Response(data: responseString);
  }

}

class DioHttpCliennt extends HttpClient {
  final dio.Dio client;

  DioHttpCliennt(this.client);
  @override
  Future<Response<String>> get(String url) async {
    var response = await client.getUri<String>(Uri.parse(url));
    var responseString = response.data;
    return Response(data: responseString);
  }

}

class HttpHttpClient extends HttpClient {
  final http.Client client;

  HttpHttpClient(this.client);

  @override
  Future<Response<String>> get(String url) async {
    var response = await client.get(Uri.parse(url));
    return Response(data: response.body);
  }
}

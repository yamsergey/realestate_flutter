import 'dart:convert';

import 'package:realestate_flutter/network/client/client.dart';
import 'package:realestate_flutter/network/client/response.dart';
import 'package:realestate_flutter/network/network.dart';

class PropertiesLoader {
  final String host;
  final HttpClient client;

  PropertiesLoader(
      {this.host = 'https://private-492e5-homegate1.apiary-mock.com',
      required this.client});

  Future<Response<SearchResponse>> list() async {
    final response = await client.get('$host/properties');
    if (!response.hasData) return Response();
    return Response(
        data: SearchResponse.fromJson(
            jsonDecode(response.data!) as Map<String, dynamic>));
  }
}

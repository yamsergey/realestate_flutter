import 'package:realestate_flutter/network/dto/external_url.dart';

class Property {
  final String country;
  final String offerType;
  final String objectCategory;
  final String currency;
  final String priceUnit;
  final String listingType;
  final int advertisementId;
  final int score;
  final String agencyId;
  final String title;
  final String street;
  final String zip;
  final String text;
  final String city;
  final String geoLocation;
  final int objectType;
  final double numberRooms;
  final int floor;
  final int surfaceLiving;
  final int surfaceUsable;
  final int sellingPrice;
  final int price;
  final int timestamp;
  final String timestampStr;
  final bool balcony;
  final int lastModified;
  final int searchInquiryTimestamp;
  final String picFilename1Small;
  final String picFilename1Medium;
  final List<String> pictures;
  final String objectTypeLabel;
  final String countryLabel;
  final String? floorLabel;
  final String? description;
  final String? agencyLogoUrl;
  final String agencyPhoneDay;
  final String contactPerson;
  final String contactPhone;
  final int interestedFormType;
  final List<ExternalUrl> externalUrls;

  Property(
      {required this.country,
      required this.offerType,
      required this.objectCategory,
      required this.currency,
      required this.priceUnit,
      required this.listingType,
      required this.advertisementId,
      required this.score,
      required this.agencyId,
      required this.title,
      required this.street,
      required this.zip,
      required this.text,
      required this.city,
      required this.geoLocation,
      required this.objectType,
      required this.numberRooms,
      required this.floor,
      required this.surfaceLiving,
      required this.surfaceUsable,
      required this.sellingPrice,
      required this.price,
      required this.timestamp,
      required this.timestampStr,
      required this.balcony,
      required this.lastModified,
      required this.searchInquiryTimestamp,
      required this.picFilename1Small,
      required this.picFilename1Medium,
      required this.pictures,
      required this.objectTypeLabel,
      required this.countryLabel,
      this.floorLabel,
      this.description,
      this.agencyLogoUrl,
      required this.agencyPhoneDay,
      required this.contactPerson,
      required this.contactPhone,
      required this.interestedFormType,
      required this.externalUrls});
  factory Property.fromJson(Map<String, dynamic> json) {
    return Property(
      country: json['country'] as String,
      offerType: json['offerType'] as String,
      objectCategory: json['objectCategory'] as String,
      currency: json['currency'] as String,
      priceUnit: json['priceUnit'] as String,
      listingType: json['listingType'] as String,
      advertisementId: json['advertisementId'] as int,
      score: json['score'] as int,
      agencyId: json['agencyId'] as String,
      title: json['title'] as String,
      street: json['street'] as String,
      zip: json['zip'] as String,
      text: json['text'] as String,
      city: json['city'] as String,
      geoLocation: json['geoLocation'] as String,
      objectType: json['objectType'] as int,
      numberRooms: double.parse(json['numberRooms'].toString()),
      floor: json['floor'] as int,
      surfaceLiving: json['surfaceLiving'] as int,
      surfaceUsable: json['surfaceUsable'] as int,
      sellingPrice: json['sellingPrice'] as int,
      price: json['price'] as int,
      timestamp: json['timestamp'] as int,
      timestampStr: json['timestampStr'] as String,
      balcony: json['balcony'] as bool,
      lastModified: json['lastModified'] as int,
      searchInquiryTimestamp: json['searchInquiryTimestamp'] as int,
      picFilename1Small: json['picFilename1Small'] as String,
      picFilename1Medium: json['picFilename1Medium'] as String,
      pictures: (json['pictures'] as Iterable<dynamic>)
          .map((dynamic item) => item as String)
          .toList(),
      objectTypeLabel: json['objectTypeLabel'] as String,
      countryLabel: json['countryLabel'] as String,
      floorLabel: json['floorLabel'] as String?,
      description: json['description'] as String?,
      agencyLogoUrl: json['agencyLogoUrl'] as String?,
      agencyPhoneDay: json['agencyPhoneDay'] as String,
      contactPerson: json['contactPerson'] as String,
      contactPhone: json['contactPhone'] as String,
      interestedFormType: json['interestedFormType'] as int,
      externalUrls: (json['externalUrls'] as Iterable<dynamic>)
          .map((dynamic item) =>
              ExternalUrl.fromJson(item as Map<String, dynamic>))
          .toList(),
    );
  }
}

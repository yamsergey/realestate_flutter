class ExternalUrl {
  final String url;
  final String type;
  final String label;

  ExternalUrl({required this.url, required this.type, required this.label});

  factory ExternalUrl.fromJson(Map<String, dynamic> json) {
    return ExternalUrl(
        url: json['url'] as String,
        type: json['type'] as String,
        label: json['label'] as String);
  }
}

import 'package:realestate_flutter/network/dto/property.dart';

class SearchResponse {
  final int resultCount;
  final int start;
  final int page;
  final int pageCount;
  final int itemsPerPage;
  final bool hasNextPage;
  final bool hasPreviousPage;
  final List<Property> items;

  SearchResponse(
      {required this.resultCount,
      required this.start,
      required this.page,
      required this.pageCount,
      required this.itemsPerPage,
      required this.hasNextPage,
      required this.hasPreviousPage,
      required this.items});

  factory SearchResponse.fromJson(Map<String, dynamic> json) {
    return SearchResponse(
        resultCount: json['resultCount'] as int,
        start: json['start'] as int,
        page: json['page'] as int,
        pageCount: json['pageCount'] as int,
        itemsPerPage: json['itemsPerPage'] as int,
        hasNextPage: json['hasNextPage'] as bool,
        hasPreviousPage: json['hasPreviousPage'] as bool,
        items: (json['items'] as Iterable<dynamic>)
            .map((dynamic item) => Property.fromJson(item as Map<String, dynamic>))
            .toList());
  }
}
